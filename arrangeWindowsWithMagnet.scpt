#! /usr/bin/osascript

-- Run With
-- chmod +x arrangeWindow.scpt
-- ./arrangeWindow.scpt

-- Requirements
-- allow permissions in System Preferences 
-- System Preferences > Security & Privacy > Privacy > Accessibility
--  tick box next to terminal, iTerm, or siriactionsd
-- Have Magnet installed

-- Keycodes: https://eastmanreference.com/complete-list-of-applescript-key-codes
-- Siri shortcuts
-- Can be run via siri shortcut using the Run Shell Script action and passing in the script path


on magnetIsInstalled()
	set appName to "Magnet"
	try
		-- succeeds if application is installed
	    set appPath to path to application appName
	on error
	    display dialog appName & " is not installed, please install from the Mac App Store." buttons {"OK"} default button 1
	    -- Open mac app store to Magnet page
	    -- https://apps.apple.com/us/app/magnet/id441258766?mt=12
		tell application "App Store"
		    activate
		    delay 1
		    open location "macappstore://itunes.apple.com/app/idid441258766"
		end tell
		-- exit the program entirely
		error number -128
	end try
end magnetIsInstalled


on countMonitors()
    tell application "System Events"
        set numMonitors to count of desktops
        return numMonitors
    end tell
end countMonitors


on activateApplication(appName)
	tell application appName
		activate
	end tell
	-- delay if the application isn't open yet 
	delay 1
end activateApplication


on activateAndMoveApplicationToCoordinates(appName, x, y)
	activateApplication(appName)
	tell application "System Events"
		-- optional, but shrink the window smooths out some issues if a window overlaps between displays
		set size of first window of application process appName to {400, 200}
	    set position of first window of application process appName to {x, y}
	end tell
	delay 1
end activateAndMoveApplicationToCoordinates


on moveLeftOneWindow()
	-- move window to left most monitor
	moveToWindow(123)
end moveLeftOneWindow


on moveRightOneWindow()
	-- move window to left most monitor
	moveToWindow(124)
end moveRightOneWindow


on moveToWindow(keyCode)
	-- move window left or right
	-- expected keyCode is either 
	-- left:  123
	-- right: 124
	tell application "System Events"
		key down {control}
		key down {option}
		key down {command}
		key down (key code keyCode)
		key up (key code keyCode)
		key up {command}
		key up {option}
		key up {control}	
	end tell
end moveToWindow


on resizeWindow(keyCode)
	tell application "System Events"
		key down {control}
		key down {option}
		key down (key code keyCode)
		key up (key code keyCode)
		key up {option}
		key up {control}
	end tell
end resizeWindow


on resizeTopLeftCorner()
	resizeWindow(32) -- u
end resizeTopLeftCorner


on resizeBottomLeftCorner()
	resizeWindow(38) -- j
end resizeBottomLeftCorner


on resizeTopRightCorner()
	resizeWindow(34) -- i
end resizeTopRightCorner


on resizeBottomRightCorner()
	resizeWindow(40) -- k
end resizeBottomRightCorner


on resizeLeftHalf()
	resizeWindow(123) -- left arrow
end resizeLeftHalf


on resizeRightHalf()
	resizeWindow(124) -- right arrow
end resizeRightHalf


on resizeFullScreen()
	resizeWindow(36) -- return
end resizeFullScreen


-- Below is a test 
on testMoveAndResize()
	activateApplication("Safari")
	moveToRightMostMonitor()
	delay 1
	moveToLeftMostMonitor()
	delay 1
	moveRightOneWindow()
	delay 1
	moveLeftOneWindow()
	delay 1
	resizeTopLeftCorner()
	delay 1
	resizeBottomLeftCorner()
	delay 1
	resizeBottomRightCorner()
	delay 1
	resizeTopRightCorner()
	delay 1
	resizeRightHalf()
	delay 1
	resizeLeftHalf()
	delay 1
	resizeFullScreen()
	tell application "Safari"
		quit
	end tell
end testMoveAndResize

------------------------------------------
-- MAIN
------------------------------------------
magnetIsInstalled()
activateApplication("Magnet")


-- The monitor set as the main display has the top left most pixel set as a coordinate of 0, 0
-- depending on where other monitors or set around that main monitor, you can add or subtract the number
-- of pixels each screen has to get the coordinate to use. When using magnet, you only need to get the window
-- to move to that next screen, then magnet can resize the window for you. 

activateAndMoveApplicationToCoordinates("Firefox", 0, -1000)
resizeFullScreen()

activateAndMoveApplicationToCoordinates("iTerm2", 0, 0)
resizeRightHalf()

activateAndMoveApplicationToCoordinates("Sublime Text", 0, 0)
resizeLeftHalf()




